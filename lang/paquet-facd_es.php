<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-facd?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'facd_description' => 'Gestión de cola de espera para la conversión de documentos.',
	'facd_slogan' => 'Gestión de cola de espera para la conversión de documentos'
);
