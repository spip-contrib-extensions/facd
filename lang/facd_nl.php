<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/facd?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_relancer_tout' => 'Converteer alle documenten opnieuw',
	'bouton_relancer_tout_format' => 'Converteer alle documenten met formaat @format@ opnieuw',
	'bouton_relancer_tout_format_message' => 'Weet je zeker dat je alle documenten in formaat @format@ opnieuw wilt encoderen?',
	'bouton_relancer_tout_message' => 'Weet je zeker dat je alle documenten opnieuw wilt encoderen?',

	// E
	'erreur_document_plus_disponible' => 'Dit document is niet meer op de site beschikbaar',
	'explication_file_attente' => 'Op deze bladzijde vind je de lijst van documenten die op conversie wachten. Ze zullen automatisch door CRON worden verwerkt.',

	// I
	'info_document_conversion' => 'Dit document wordt momenteel geconverteerd',
	'info_document_conversion_erreur' => 'De conversie van dit document is mislukt',
	'info_document_dans_file_attente' => 'Wachtend op conversie',
	'info_relancer_erreurs' => 'Start de misluke conversies opnieuw',
	'info_statut_conversion_en_cours' => 'Bezig',
	'info_statut_conversion_erreur' => 'Fout',
	'info_statut_conversion_non' => 'Wachtend',
	'info_statut_conversion_oui' => 'Geconverteerd',
	'info_tous_docs_facd' => 'Documenten in de wachtrij',
	'info_voir_log_erreur' => 'Kijk naar de foutgeschiedenis',

	// L
	'label_relancer_conversion' => 'De conversie herstarten',
	'lien_convertir_document' => 'Dit document converteren',
	'lien_recharger' => 'Opnieuw laden',
	'liste_attente_aucun' => 'Geen enkel document in de wachtrij.',
	'liste_attente_tous' => 'Er staan documenten in de wachtrij',
	'liste_convert_aucun' => 'Geen enkel document geconverteerd',
	'liste_convert_tous' => 'Documenten geconverteerd',

	// T
	'thead_date' => 'Datum',
	'thead_duree' => 'Duur',
	'thead_duree_conversion' => 'Duur van de conversie',
	'thead_extension' => 'Formaat',
	'thead_fonction' => 'Gebruikte functie',
	'thead_id' => 'ID',
	'thead_id_auteur' => 'Auteur',
	'thead_id_document' => 'Origineel',
	'thead_nombre' => 'Aantal',
	'thead_statut' => 'Status',
	'titre_log_conversion' => 'Inhoud van conversielog #@id@',
	'titre_page_file' => 'Wachtrij van de conversiemodule voor media',
	'titre_page_file_convertis_jour' => 'Conversies per datum',
	'titre_page_file_menu' => 'Wachtrij van de conversie',

	// V
	'version_encodee_de' => 'Dit document si een conversie van document @id_orig@'
);
